#!/bin/bash
# Ready the system - run as root

mkdir -p /var/lib/drone
yum install docker -y
curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose 

# Manually edit .env file

echo -e "DRONE_HOST=https://drone.crinkle.ninja\nGITLAB_CLIENT=\nGITLAB_SECRET=\nGITLAB_URL=https://gitlab.com\nDRONE_SECRET=\n" >> .env

# run: screen -dmS drone docker-compose up

